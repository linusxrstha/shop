from django.contrib import admin
from .models import Logo, Item, SubCategory

# Register your models here.
admin.site.register(Logo)
admin.site.register(Item)
admin.site.register(SubCategory)
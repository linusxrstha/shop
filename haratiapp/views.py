from django.shortcuts import render, HttpResponse, HttpResponseRedirect, reverse
from django.views import View
from .models import Logo, Item
from .forms import LoginForm, SignUpForm, AddProduct
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.generic import TemplateView, ListView, CreateView
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import EmailMessage
from haratiapp.tokens import account_activation_token
from django.utils.encoding import force_bytes, force_text
from django.urls import reverse_lazy

from django.contrib.auth import get_user_model

# Create your views here.

class Index(View):
    def get(self, request):
        logo = Logo.objects.all()[1]
        form = LoginForm()
        rform = SignUpForm()
        user = get_user_model()
        print(user.username)
        return render(request, 'homepage.html', {'obj':logo, 'loginform': form , 'register':rform} )

    def post(self, request):
        form = LoginForm(request.POST)
        register = SignUpForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['loginusername']
            password = form.cleaned_data['loginpassword']
            user = authenticate(username=username, password=password)


            if user is not None:
                login(self.request, user)
                print('User logged in !!')
                if user.is_superuser:
                    messages.success(self.request, 'Soooooooper USSERRRRRRRRRR !!')
                    return HttpResponseRedirect(reverse('haratiapp:index'))
                else:
                    messages.success(self.request, 'Login Successful !!')
                    return HttpResponseRedirect(reverse('haratiapp:index'))

            else:
                messages.warning(self.request, 'Please check your username and password !!')
                return HttpResponseRedirect(reverse('haratiapp:index'))


        elif register.is_valid():
            user = register.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your MySite Account'
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = register.cleaned_data.get('email')

            email = EmailMessage(
                subject, message, to=[to_email]

            )
            email.send()
            messages.info(request, 'Email has been sent !!')
            return HttpResponseRedirect(reverse('haratiapp:index'))


        else:
            return HttpResponse('Null')



class Logout(TemplateView):
    def get(self, request):
        logout(request)
        messages.info(request, 'Logged Out !!')
        return HttpResponseRedirect(reverse('haratiapp:index'))


class SignUp(View):
    def get(self, request):
        form = SignUpForm()
        return render(request, 'register.html', {'register':form})

    def post(self, request):
        print("signup post method called")
        form = SignUpForm(request.POST)
        if form.is_valid():
            print('form valid !!')
        return reverse('haratiapp:index')




def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)

        return render(request, 'activate.html')
    else:
        return HttpResponse('Activation link is invalid!')


class Clothing(ListView):
    model = Item
    template_name = 'clothing.html'

    def get_context_data(self, **kwargs):
        context  = super().get_context_data(**kwargs)
        context['active'] = 'clothing'
        return context



class Footwear(ListView):
    model = Item
    template_name = 'footwear.html'

    def get_context_data(self, **kwargs):
        context  = super().get_context_data(**kwargs)
        context['active'] = 'footwear'
        return context


class BagsAndWallets(ListView):
    model = Item
    template_name = 'bagsandwallets.html'

    def get_context_data(self, **kwargs):
        context  = super().get_context_data(**kwargs)
        context['active'] = 'bagsandwallets'
        return context


class WomensWear(ListView):
    model = Item
    template_name = 'womenswears.html'

    def get_context_data(self, **kwargs):
        context  = super().get_context_data(**kwargs)
        context['active'] = 'womenswear'
        return context


class Accessories(ListView):
    model = Item
    template_name = 'accessories.html'

    def get_context_data(self, **kwargs):
        context  = super().get_context_data(**kwargs)
        context['active'] = 'accessories'
        return context





################ ADMIN CREATE/LIST #############################

from django.conf.urls import url
from .views import Index, Logout, SignUp,activate, Clothing, Footwear, WomensWear, BagsAndWallets, Accessories

app_name = 'haratiapp'
urlpatterns = [
    url(r'^$', Index.as_view(), name = 'index'),
    url(r'^logout$', Logout.as_view(), name='logout'),
    url(r'^signup$', SignUp.as_view(), name='signup'),
    url(r'^clothing$', Clothing.as_view(), name='clothing'),
    url(r'^footwear', Footwear.as_view(), name='footwear'),
    url(r'^womenswear', WomensWear.as_view(), name='womenswear'),
    url(r'^bagsandwallets', BagsAndWallets.as_view(), name='bagsandwallets'),
    url(r'^accessories', Accessories.as_view(), name='accessories'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),

]

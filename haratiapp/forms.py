from django import forms
from .models import Item
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    loginusername = forms.CharField(
    widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Enter Email',
        }
    )
    )
    loginpassword = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Enter Password',
        }
    ))


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Enter your firstname'
            }
        ), required=False, max_length=30, help_text='Optional'
    )
    last_name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Enter your lastname'
            }
        ), required=False, max_length=30, help_text='Optional'
    )

    email = forms.EmailField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Email address'
            }
        ), required=True, max_length=254, help_text='Required. Inform a valid email address.'
    )
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Enter your username'
            }
        ), required=True, max_length=30, help_text='Required. Inform a valid email username.'
    )

    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Enter your Password'
            }
        ), required=True, max_length=30, help_text='Required. (Min 8 characters)'
    )

    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Confirm your password.'
            }
        ), required=True, max_length=30, help_text='Confirm your password.'
    )


    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )



class AddProduct(forms.ModelForm):
   class Meta:
        model = Item
        fields = ['name', 'description',
                  'item_number', 'size', 'color', 'subcategory', 'price',
                  'status']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter product name',
                'required': 'True',
                            }),
            'description': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter description of your product.',

                            }),
            'item_number': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter item number',

            }),
            'size': forms.Select(attrs={
                'class': 'form-control borderline',
                'placeholder': 'Enter Size',


            }),
             'color': forms.Select(attrs={
                 'class': 'form-control',
                 'placeholder': 'Color',

             }),


            'subcategory': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Subcategory',

            }),

            'price': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Price',

            }),

            'status': forms.Select(attrs={
                'class': 'form-control',
              }),
        }


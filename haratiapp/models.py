from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.shortcuts import reverse

category = (
    ('Clothing','Clothing'), ('FootWear', 'FootWear'), ('Bags&Wallets', 'Bags&Wallets'), ('Accessories', 'Accessories'),
    ('WomenWears','WomenWears')
)


subcategory = (
    ('T-shirt','T-shirt'), ('Casual Shirt', 'Casual Shirt'), ('Half Pants', 'Half Pants'),
    ('Jeans','Jeans'),
    ('Trousers','Trousers'),
    ('Sportswear', 'Sportswear'),
    ('Sweaters', 'Sweaters'),
    ('Boxers', 'Boxers'),
    ('Thermal','Thermal'),
    ('Windcheater','Windcheater')
)


size = (
    ('Double-Extra-Large', 'XXL'),
    ('Extra-Large', 'XL'),
    ('Large', 'L'),
    ('Medium', 'M'),
    ('Small', 'S'),
)
# Create your models here.

quantity = (
    (1, '1'),
    (2, '2'),
    (3,'3'),
    (4,'4'),
    (5,'5'),
    ( 6,'6'),
    (7,'7'),(8,'8'),(9,'9'),(10, '10')
)

color = (
    ('Red','Red'),
    ('Blue', 'Blue'),
    ('Black', 'Black'),
    ('Yellow', 'Yellow'),
    ('White', 'White'),
    ('Green', 'Green')

)

status = (
    (1, 'Yes'),
    (0, 'No')
)

class Time(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, )
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True, )
    deleted_at = models.DateTimeField(null=True, blank=True)
    class Meta:
        abstract = True

    def delete(self,*args,**kwargs):
        self.deleted_at = timezone.now()
        super().save()

################################################# NEW CONCEPT ############################################################



class SubCategory(models.Model):
    category=models.CharField(choices=category, max_length=100)
    subcategory=models.CharField(max_length=100)

    def __str__(self):
        return self.subcategory

class Item(Time):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True,null=True)
    item_number = models.CharField(max_length=25,blank=True,null=True)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE, blank=True, null=True)
    size = models.CharField(choices=size, max_length=25,blank=True,null=True)
    color = models.CharField(choices=color, max_length=25,blank=True,null=True)
    price = models.IntegerField(blank=True,null=True)
    status = models.BooleanField(choices=((True,'Publish'),(False,'Dont Publish')), default=False)
    photo = models.ImageField(upload_to='media_cdn', blank=True,null=True)
    slug = models.SlugField(blank=True)
    like = models.ManyToManyField(User, blank = True)
    size_slug = models.SlugField(blank=True)

    def __str__(self):
        return self.name


    def save(self, *args, **kwargs):
        self.slug = slugify(self.subcategory.category)
        self.size_slug = slugify(self.size)
        super(Item, self).save(*args, **kwargs)


    class Meta:
        ordering = ('-id',)

    def get_absoulte_url(self):
        return reverse('haratiapp:detail_category', kwargs={'pk':self.pk})

    def like_count(self):
        return self.like.count()



class Featured(models.Model):
    featured = models.ForeignKey(Item)
    def __str__(self):
        return self.featured.name


class Trending(models.Model):
    trending = models.ForeignKey(Item)
    def __str__(self):
        return self.trending.name


class Latest(models.Model):
    latest = models.ForeignKey(Item)
    def __str__(self):
        return self.latest.name


class Logo(models.Model):
    logo = models.ImageField(upload_to='images')











